from django.shortcuts import render
from .models import Perro,Persona
from django.contrib.auth import authenticate,logout, login as auth_login
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from rest_framework import viewsets
from .serializers import PersonaSerializer,PerroSerializer

# Create your views here.
def index(request):
    usuario = request.session.get('usuario',None)
    return render(request, 'index/index.html', {'perros':Perro.objects.all(),'usuario':usuario})

@login_required
def registrarPerro(request):
    nombre = request.POST.get('nombre','')
    raza = request.POST.get('raza','')
    estado = request.POST.get('estado','')
    foto = request.FILES['foto']
    descripcion = request.POST.get('descripcion','')
    perro = Perro(nombre=nombre,raza=raza,estado=estado,foto=foto,descripcion=descripcion)
    perro.save()
    return redirect('administrar')

@login_required
def editar_perro(request):
    nombre = request.POST.get('nombre','')
    raza = request.POST.get('raza','')
    estado = request.POST.get('estado','')
    foto = request.FILES['foto']
    descripcion = request.POST.get('descripcion','')
    id = request.POST.get('id',0)
    perro = Perro.objects.get(pk = id)
    perro.nombre = nombre
    perro.raza = raza
    perro.estado = estado
    perro.foto = foto
    perro.save()
    return redirect('index')

def registrarUsuario(request):
    nombre = request.POST.get('nombre','')
    correo = request.POST.get('correo','')
    run = request.POST.get('run','')
    numero = request.POST.get('numero')
    region = request.POST.get('region','')
    ciudad = request.POST.get('comuna','')
    fecha = request.POST.get('fecha','')
    tipoVivienda = request.POST.get('tipoVivienda','')
    u = User.objects.create_user(nombre,correo,run)
    u.save()

    persona = Persona(nombre=nombre,correo=correo,run=run,numero=numero,region=region,ciudad=ciudad,fecha_nacimiento=fecha,tipo_vivienda=tipoVivienda)
    persona.save()
    return redirect('index')

def login_page(request):
    return render(request,'index/login.html',{})

def login(request):
    usuario = request.POST.get('usuario','') 
    contra = request.POST.get('contra','')
    u = authenticate(request,username=usuario,password=contra)

    if u is not None:
        auth_login(request, u)
        request.session['usuario'] = u.username
        return redirect("index")
    else:
        return redirect("login_page")

@login_required
def cerrar_sesion(request):
    logout(request)
    return redirect("index")

@login_required
def administrar(request):
    if request.user.is_authenticated:
        usuario = request.user
    if usuario.is_staff:
        return render(request,'index/administracion.html',{'perros':Perro.objects.all()})
    else:
        return redirect("index")

@login_required
def borrar_perro(request,id):
    perro = Perro.objects.get(pk = id)
    perro.delete()
    return redirect('administrar')

class PersonaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Persona.objects.all()
    serializer_class = PersonaSerializer

class PerroViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Perro.objects.all()
    serializer_class = PerroSerializer