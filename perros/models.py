from django.db import models

# Create your models here.
class Perro(models.Model):
    nombre = models.CharField(max_length=40)
    raza = models.CharField(max_length=40)
    descripcion = models.CharField(max_length=100)
    estado = models.CharField(max_length=40)
    foto = models.ImageField(upload_to='perros')

    def __str(self):
        return "Perro"

class Persona(models.Model):
    nombre = models.CharField(max_length=40)
    correo = models.EmailField()
    run = models.CharField(max_length=13)
    fecha_nacimiento = models.DateField()
    numero = models.IntegerField()
    region = models.CharField(max_length=20)
    ciudad = models.CharField(max_length=20)
    tipo_vivienda = models.CharField(max_length=40)

    def __str(self):
        return "Persona"