var CACHE_NAME = 'MisPerrisCache';
var urlsToCache = [
    '/',
    '/api/perros',
    '/static/js/app.js',
    '/static/js/administrar.js',
    '/static/js/bootstrap.js',
    '/static/js/bootstrap.bundle.js',
    '/static/js/jquery-3.3.1 .js',
    '/static/js/jquery-validate.bootstrap-tooltip.js',
    '/static/js/jquery.validate.js',
    '/static/js/popper.js',
    '/static/css/bootstrap-grid.css',
    '/static/css/bootstrap-reboot.css',
    '/static/css/bootstrap.css',
    '/static/css/estilo.css',
    '/static/img/crowfunding.jpg',
    '/static/img/icono_facebook.png',
    '/static/img/icono_gmail.png',
    '/static/img/icono_instagram.png',
    '/static/img/icono_mensaje.png',
    '/static/img/logo.png',
    '/static/img/perro.png',
    '/static/img/rescate.jpg',
    '/static/img/adoptados/Duque.jpg',
    '/static/img/adoptados/Apolo.jpg',
    'https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.js',
    'https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js',
    'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css',
    'https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/css/inputmask.min.css',
    '/subir/perros/Bigotes.jpg', 
    '/subir/perros/Chocolate.jpg', 
    '/subir/perros/Luna.jpg', 
    '/subir/perros/Maya.jpg', 
    '/subir/perros/Oso.jpg',
    '/subir/perros/Pexel.jpg', 
    '/subir/perros/Wifi.jpg'
];

self.addEventListener('install', function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Cache open!');
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request)
        .then(function(response) {
            // Cache hit - return response
            if (response) {
                return response;
            }
            return fetch(event.request);
        })
    );
});

self.addEventListener('activate', event => {
    // remove old caches
    event.waitUntil(
      caches.keys().then(keys => Promise.all(
        keys.map(key => {
          if (key != CACHE_NAME) {
            return caches.delete(key);
          }
        })
      )).then(() => {
        console.log('Now ready to handle fetches!');
      })
    );
});